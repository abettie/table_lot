
// DOM読み込み待ち
$(function () {
    // クッキー保存キーのプレフィックス
    const cookiePrefix = 'table_'
    // 席埋めボタンidのプレフィックス
    const filledPrefix = 'filled_'
    // 席空けボタンidのプレフィックス
    const remainPrefix = 'remain_'
    // 結果表示
    const domResult = $('#result')
    // くじ引きボタン
    const domPickupButton = $('#button-pickup')
    // 残り席数表示
    const domRemain = $('#remain')
    // オールリセットボタン
    const domAllResetButton = $('#button-all-reset')

    // 抽選対象テーブルと席数
    let tablesInfo = []
    // 抽選残存テーブル
    let remainingTableIds = []
    // 直近の抽選結果
    const latestResults = []
    const latestResultsMaxLength = 5

    // 各テーブルの席数をjsonファイルから取得
    $.getJSON('data/table.json', function (data) {
        tablesInfo = data
        // 残存席数を表示
        tablesInfo.forEach(function (val, idx) {
            domRemain.append(
                '<div class="row">' +
                '<div class="col-8">' +
                'テーブル' + val.name + '：' +
                '<span id="' + filledPrefix + idx + '" class="filled-amount">0</span>' +
                '<span id="' + remainPrefix + idx + '" class="remain-amount">(残り' + val.numberOfSeats + ')</span>' +
                '</div>' +
                '<div class="col-2">' +
                '<button data-id="' + idx + '" class="add-count btn btn-outline-info">埋</button>' +
                '</div>' +
                '<div class="col-2">' +
                '<button data-id="' + idx + '" class="red-count btn btn-outline-danger">空</button>' +
                '</div>'
            )
        })
        $('.add-count').on('click', function () {
            fillOneSeat($(this).data('id'))
            updateRemainingTables()
        })
        $('.red-count').on('click', function () {
            emptyOneSeat($(this).data('id'))
            updateRemainingTables()
        })
        updateRemainingTables()
    })

    // イベント設定
    domPickupButton.on('click', function () {
        doLot()
    })
    domAllResetButton.on('click', function () {
        // 確認した後に全ての数値を0に
        const answer = confirm('全ての数値をリセットします。よろしいですか？')
        if (answer === true) {
            tablesInfo.forEach(function (val, idx) {
                $.cookie(cookiePrefix + idx, 0)
            })
            updateRemainingTables()
        }
    })

    /**
     * 対象テーブルの座席を1つ埋める
     *
     * @param {number} tableId
     * @returns {boolean}
     */
    function fillOneSeat (tableId) {
        let nowCount = $.cookie(cookiePrefix + tableId)
        if (typeof nowCount === 'undefined') {
            nowCount = 0
        } else {
            nowCount = Number(nowCount)
        }
        if (nowCount < tablesInfo[tableId].numberOfSeats) {
            $.cookie(cookiePrefix + tableId, nowCount + 1)
        }
        return true
    }

    /**
     * 対象テーブルの座席を1つ空ける
     *
     * @param tableId
     * @returns {boolean}
     */
    function emptyOneSeat (tableId) {
        let nowCount = $.cookie(cookiePrefix + tableId)
        if (typeof nowCount === 'undefined') {
            nowCount = 0
        } else {
            nowCount = Number(nowCount)
        }
        if (nowCount > 0) {
            $.cookie(cookiePrefix + tableId, nowCount - 1)
        }
        return true
    }

    // 現在残っている座席数リストを更新
    function updateRemainingTables () {
        remainingTableIds = []

        tablesInfo.forEach(function (val, idx) {
            let filledSeatsCount = $.cookie(cookiePrefix + idx)
            if (typeof filledSeatsCount === 'undefined') {
                filledSeatsCount = 0
            }
            if (filledSeatsCount < val.numberOfSeats) {
                // まだテーブルの席が埋まっていないので抽選対象に
                remainingTableIds.push(idx)
            }
            // 表示更新
            $('#' + filledPrefix + idx).text(filledSeatsCount)
            $('#' + remainPrefix + idx).text('(残り' + (val.numberOfSeats - filledSeatsCount) + '席)')
        })
        // 表示更新
        latestResults.forEach(function (val, idx) {
            $('#latest div:nth-child(' + (idx + 2) + ')').text(tablesInfo[val].name)
        })
    }

    // くじ引き
    function doLot () {
        const id = Math.floor(Math.random() * remainingTableIds.length)
        const tableId = remainingTableIds[id]
        domResult.css('opacity', '0')
        domResult.text(tablesInfo[tableId].name)
        domResult.animate({opacity: 1}, {duration: 500})
        fillOneSeat(tableId)
        latestResults.unshift(tableId)
        if (latestResults.length > latestResultsMaxLength) {
            latestResults.pop()
        }
        updateRemainingTables()
    }
})
